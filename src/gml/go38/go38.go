package main

import "code.google.com/p/go-tour/pic"
import "math"

func Pic(dx, dy int) [][]uint8 {
    r := make([][]uint8, dy)
    for y := range r {
        r[y] = make([]uint8, dx)
        for x := range(r[y]) {
            fx := float64(x-dx/2)
            fy := float64(y-dy/2)
        	r[y][x] = uint8(int(math.Sqrt(fx*fx+fy*fy))*256/(dx+dy))   
        }
    }
    
    return r
}

func main() {
    pic.Show(Pic)
}